using System;
using System.IO;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Text.Json.Nodes;
using System.Threading.Tasks;
using System.Linq;

namespace ntfy_desktop {
	public class NTFYD {
		public event EventHandler MessageReceived;

		private AppConfig Settings => AppSettings.Default;
		private DebugLog debugLog;
		private IDictionary<Feed, Task> currentFeeds = new Dictionary<Feed, Task>();

		public NTFYD(DebugLog dl) {
			debugLog = dl;

			UpdateSubscriptions();

			AppSettings.Updated += (sender, e) => UpdateSubscriptions();
		}

		public void UpdateSubscriptions() {
			DisposeAll();
			Settings.Feeds.ForEach((feed) => currentFeeds.Add(Subscribe(feed)));
		}

		public KeyValuePair<Feed, Task> Subscribe(Feed feed) {
			var task = GetSubscribeThread(feed);
			task.ContinueWith((t) => {
				if (t.Exception == null)
					debugLog.Log($"← closed {feed.ToUrl()}");
				else
					debugLog.Log($"! {t.Exception.GetType()}: {t.Exception.Message} @ {t.Exception.StackTrace}");
			}, TaskContinuationOptions.OnlyOnFaulted);
			task.ContinueWith((t) => {
				debugLog.Log($"← unsubscribed from {feed.ToUrl()}");
			}, TaskContinuationOptions.OnlyOnRanToCompletion);
			task.Start();
			return new KeyValuePair<Feed, Task>(feed, task);
		}

		private Task GetSubscribeThread(Feed feed) {
			return new Task(() => {
				using (TcpClient client = new TcpClient()) {
					// generate request header
					string requestString = $"GET /{feed.Topic}/json HTTP/1.1\n"
						+ $"Host: {feed.Domain}\n"
						+ "Connection: keep-alive\n\n";

					client.Connect(feed.Domain, 80);

					try {
						using (NetworkStream stream = client.GetStream()) {
							// send request
							StreamWriter writer = new StreamWriter(stream);
							writer.Write(requestString);
							writer.Flush();

							// process response
							StreamReader rdr = new StreamReader(stream);
							debugLog.Log($"← subscribed to {feed.ToUrl()}");

							while (!rdr.EndOfStream) {
								var line = rdr.ReadLine();
								if (!line.StartsWith("{") || !line.EndsWith("}") || !line.Contains("\"event\":\"message\""))
									continue;

								var o = JsonNode.Parse(line);

								OnMessageReceived(new MessageReceivedEventArgs {
									feed = feed,
									json = o,
								});
							}
						}
					} catch (Exception e) {
						if (e.GetType() != typeof(System.IO.IOException))
							debugLog.Log($"! {e.GetType()}: {e.Message} @ {e.StackTrace}");
					}
				}
			});
		}

		public void DisposeAll() {
			foreach (var item in currentFeeds) {
				item.Value.Dispose();
			}
			currentFeeds.Clear();
		}

		protected virtual void OnMessageReceived(MessageReceivedEventArgs e) {
			MessageReceived?.Invoke(this, e);
		}
	}

	public class MessageReceivedEventArgs : EventArgs {
		public Feed feed { get; set; }
		public JsonNode json { get; set; }
	}
}
